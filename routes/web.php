<?php

use App\Http\Controllers\AuthorController;
use App\Http\Controllers\BookController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('home');
});

Route::get('authors', [AuthorController::class, 'index'])->name('authors.index');
Route::post('authors', [AuthorController::class, 'create']);
Route::post('authors/delete', [AuthorController::class, 'delete'])->name('authors.delete');
Route::post('authors/update', [AuthorController::class, 'update'])->name('authors.update');
Route::get('authors/search', [AuthorController::class, 'search'])->name('authors.search');



Route::get('books', [BookController::class, 'index'])->name('books.index');
Route::post('books', [BookController::class, 'create']);
Route::post('books/delete', [BookController::class, 'delete'])->name('books.delete');
Route::post('books/update', [BookController::class, 'update'])->name('books.update');
Route::get('books/search', [BookController::class, 'search'])->name('books.search');

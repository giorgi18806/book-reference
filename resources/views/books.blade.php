@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="d-flex justify-content-between my-3">
            <h3 class="panel-title text-success">
                List of books
            </h3>
            <div class="col-lg-4">
                <input type="text" class="form-control" name="searchItem" id="searchItem" placeholder="search">
            </div>
            <a href="#" class="btn btn-success" id="plusBttn" data-toggle="modal" data-target="#myModal">
                Create new book
            </a>
        </div>
        <table class="table table-striped table-success" id="divPanel">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col" id="theadTitle">Book title</th>
                <th scope="col">Description</th>
                <th scope="col">Author</th>
                <th scope="col">Photo</th>
                <th scope="col">Published at</th>
                <input type="hidden" id="inputOrderTitle" value="asc">
            </tr>
            </thead>
            <tbody id="table1">
            @foreach($books as $book)
                <tr class="currentBook" data-toggle="modal" data-target="#myModal">
                    <th scope="row">{{ $book->id }}</th>
                    <td id="currentTitle">{{ $book->title }}</td>
                    <td id="currentDescription">{{ $book->description }}</td>
{{--                    @dd($book->authors)--}}
                    <td id="currentAuthor">
                        @foreach($book->authors as $author)
                            <pre>{{ $author->last_name }}</pre>
                        @endforeach
                    </td>
                    <td id="currentPhoto">{{ $book->photo }}</td>
                    <td id="currentPublished">{{ $book->published_at }}</td>
                    <input type="hidden" id="tableBookId" value="{{ $book->id }}">
                </tr>
            @endforeach
            </tbody>
        </table>

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="modalTitle">Create new book</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <p><input type="text" id="inputBookTitle" class="form-control" placeholder="Enter book title here"></p>
                        <p><input type="text" id="inputBookDescription" class="form-control" placeholder="Enter book description here"></p>
{{--                        <p><input type="text" id="inputBookAuthor" class="form-control" placeholder="Enter book author here"></p>--}}
                        <select name="inputBookAuthor" id="inputBookAuthor" class="form-control mb-3" multiple>
                            <option value="" disabled>Select author</option>
                            @foreach($authors as $author)
                                <option class="form-control" value="{{ $author->id }}">{{ $author->first_name }} {{ $author->last_name }}</option>
                            @endforeach
                        </select>
                        <p><input type="file" id="inputBookPhoto" name="photo" class="form-control" placeholder="Enter book photo here"></p>
                        <select name="inputBookPublished" id="inputBookPublished" class="form-control" >
                            <option value="" disabled>Select the year of publication</option>
                            @for($i = 2021; $i>1950; $i--)
                                <option class="form-control" value="{{ $i }}">{{ $i }}</option>
                            @endfor
                        </select>
                        <input type="hidden" id="modalBookId">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" id="deleteBttn" data-dismiss="modal" style="display: none">Delete</button>
                        <button type="button" class="btn btn-dark" id="saveChangesBttn" data-dismiss="modal" style="display: none">Save changes</button>
                        <button type="button" class="btn btn-dark" id="addAuthorBttn" data-dismiss="modal">Add author</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
    {{ csrf_field() }}
@endsection
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA==" crossorigin="anonymous"></script>
<script>
    $(document).ready(function() {
        $(document).on('click', '.currentBook', function () {
            var text = $(this).text();
            text.replace(/\s/g, '');
            var id = $(this).find('#tableBookId').val();
            $('#modalTitle').text('Edit book');
            $('#inputBookTitle').val($(this).find('#currentTitle').text());
            $('#inputBookDescription').val($(this).find('#currentDescription').text());
            // $('#inputBookAuthor').val($(this).find('#currentAuthor').text());
            $('#inputBookPhoto').val($(this).find('#currentPhoto').text());
            $('#inputBookPublished').val($(this).find('#currentPublished').text());
            $('#deleteBttn').show('200');
            $('#saveChangesBttn').show('200');
            $('#addAuthorBttn').hide('200');
            $('#modalBookId').val(id);
            console.log(text);
        });

        $(document).on('click', '#plusBttn', function (event) {
            $('#modalTitle').text('Create new book');
            $('#inputBookTitle').val("");
            $('#inputBookDescription').val("");
            $('#inputBookAuthor').val("");
            $('#inputBookPhoto').val("");
            $('#inputBookPublished').val("");
            $('#deleteBttn').hide('200');
            $('#saveChangesBttn').hide('200');
            $('#addAuthorBttn').show('200');
        });

        $('#addAuthorBttn').click(function (event) {
            var title = $('#inputBookTitle').val();
            var description = $('#inputBookDescription').val();
            var author = $('#inputBookAuthor').val();
            var photo = $('#inputBookPhoto').val();
            var published = $('#inputBookPublished').val();
            if(title == "" || author == "" || photo == "" || published == "") {
                alert("This field cannot be empty")
            }else{
                $.post(
                    'books',
                    {
                        'title': title,
                        'description': description,
                        'author_id': author,
                        'photo': photo,
                        'published_at': published,
                        '_token': $('input[name=_token]').val()
                    },
                    function (data) {
                        console.log(data);
                        $('#divPanel').load(location.href + ' #divPanel')
                    }
                );
            }
        });

        $('#deleteBttn').click(function (event) {
            var id = $('#modalBookId').val();
            $.post('{{ route('books.delete') }}', {'id': id, '_token': $('input[name=_token]').val()}, function (data) {
                console.log(data);
                $('#divPanel').load(location.href + ' #divPanel')
            })
        });

        $('#saveChangesBttn').click(function (event) {
            var id = $('#modalBookId').val();
            var text = $('#inputBookTitle').val();
            var author_id =$('#inputBookAuthor').val();
            $.post('{{ route('books.update') }}', {'id': id, 'title': text, 'author_id': author_id, '_token': $('input[name=_token]').val()}, function (data) {
                console.log(data);
                $('#divPanel').load(location.href + ' #divPanel')
            })
        });

        $( function() {
            $( "#searchItem" ).autocomplete({
                source: '{{ route('books.search') }}'
            });
        } );

        $(document).on('click', '#theadTitle', function orderName()
        {
            var table=$('#divPanel');
            var tbody =$('#table1');

            tbody.find('tr').sort(function(a, b)
            {
                console.log('Giorgi');
                if($('#inputOrderTitle').val()=='asc')
                {
                    return $('td:first', a).text().localeCompare($('td:first', b).text());
                }
                else
                {
                    return $('td:first', b).text().localeCompare($('td:first', a).text());
                }
            }).appendTo(tbody);

            var sort_order=$('#inputOrderTitle').val();
            if(sort_order=="asc")
            {
                document.getElementById("inputOrderTitle").value="desc";
            }
            if(sort_order=="desc")
            {
                document.getElementById("inputOrderTitle").value="asc";
            }
        });
    });
</script>


<div class="container">
    <nav class="navbar navbar-expand-md navbar-dark bg-dark">


        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav me-auto mb-2 mb-md-0">
                <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
                    {{--                        @dd(Request::route()->uri)--}}
                    <a class="nav-link " href="/">Home</a>
                </li>
                <li class="nav-item {{ Request::is('books') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('books.index') }}">Books</a>
                </li>
                <li class="nav-item {{ Request::is('authors') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('authors.index') }}">Authors</a>
                </li>
            </ul>

        </div>

    </nav>
</div>

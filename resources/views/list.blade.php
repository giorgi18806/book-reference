@extends('layouts.app')

@section('content')
<div class="container">
            <div class="d-flex justify-content-between my-3">
            <h3 class="panel-title text-success">
                List of authors
            </h3>
            <div class="col-lg-4">
                <input type="text" class="form-control" name="searchItem" id="searchItem" placeholder="search">
            </div>
            <a href="#" class="btn btn-success" id="plusBttn" data-toggle="modal" data-target="#myModal">
                Create new author
            </a>
            </div>
        <table class="table table-striped table-success" id="divPanel">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">First name</th>
                        <th scope="col">Second name</th>
                        <th scope="col" id="theadLast" >Last name</th>
                        <input type="hidden" id="inputOrderLastName" value="asc">
                    </tr>
                </thead>
                <tbody id="table1">
                    @foreach($authors as $author)
                        <tr class="currentAuthor" data-toggle="modal" data-target="#myModal">
                            <th scope="row">{{ $author->id }}</th>
                            <td id="currentFirst">{{ $author->first_name }}</td>
                            <td id="currentSecond">{{ $author->second_name }}</td>
                            <td id="currentLast">{{ $author->last_name }}</td>
                            <input type="hidden" id="tableAuthorId" value="{{ $author->id }}">
                        </tr>
                    @endforeach
                </tbody>
            </table>

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="modalTitle">Create new author</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <p><input type="text" id="inputAuthorFirstName" class="form-control" placeholder="Enter author first name here"></p>
                        <p><input type="text" id="inputAuthorSecondName" class="form-control" placeholder="Enter author second name here"></p>
                        <p><input type="text" id="inputAuthorLastName" class="form-control" placeholder="Enter author last name here"></p>
                        <input type="hidden" id="modalAuthorId">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" id="deleteBttn" data-dismiss="modal" style="display: none">Delete</button>
                        <button type="button" class="btn btn-dark" id="saveChangesBttn" data-dismiss="modal" style="display: none">Save changes</button>
                        <button type="button" class="btn btn-dark" id="addAuthorBttn" data-dismiss="modal">Add author</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
</div>
{{ csrf_field() }}
@endsection
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA==" crossorigin="anonymous"></script>
<script>
    $(document).ready(function() {
        $(document).on('click', '.currentAuthor', function () {
            var text = $(this).text();
            text.replace(/\s/g, '');
            var id = $(this).find('#tableAuthorId').val();
            $('#modalTitle').text('Edit author');
            $('#inputAuthorFirstName').val($(this).find('#currentFirst').text());
            $('#inputAuthorSecondName').val($(this).find('#currentSecond').text());
            $('#inputAuthorLastName').val($(this).find('#currentLast').text());
            $('#deleteBttn').show('200');
            $('#saveChangesBttn').show('200');
            $('#addAuthorBttn').hide('200');
            $('#modalAuthorId').val(id);
            console.log($(this).find('#currentLast').text());
        });

        $(document).on('click', '#plusBttn', function (event) {
            $('#modalTitle').text('Create new author');
            $('#inputAuthorFirstName').val("");
            $('#inputAuthorSecondName').val("");
            $('#inputAuthorLastName').val("");
            $('#deleteBttn').hide('200');
            $('#saveChangesBttn').hide('200');
            $('#addAuthorBttn').show('200');
        });

        $('#addAuthorBttn').click(function (event) {
            var firstName = $('#inputAuthorFirstName').val();
            var secondName = $('#inputAuthorSecondName').val();
            var lastName = $('#inputAuthorLastName').val();
            if(firstName == "" || lastName=="") {
                alert("This field cannot be empty")
            }else{
                $.post(
                    'authors',
                    {
                        'first_name': firstName,
                        'second_name': secondName,
                        'last_name': lastName,
                        '_token': $('input[name=_token]').val()
                    },
                    function (data) {
                        console.log(data);
                        $('#divPanel').load(location.href + ' #divPanel')
                    }
                );
            }
        });

        $('#deleteBttn').click(function (event) {
            var id = $('#modalAuthorId').val();
            $.post('{{ route('authors.delete') }}', {'id': id, '_token': $('input[name=_token]').val()}, function (data) {
                console.log(data);
                $('#divPanel').load(location.href + ' #divPanel')
            })
        });

        $('#saveChangesBttn').click(function (event) {
            var id = $('#modalAuthorId').val();
            var first = $('#inputAuthorFirstName').val();
            var second = $('#inputAuthorSecondName').val();
            var last = $('#inputAuthorLastName').val();
            $.post('{{ route('authors.update') }}', {
                'id': id,
                'first_name': first,
                'second_name': second,
                'last_name': last,
                '_token': $('input[name=_token]').val()}, function (data) {
                console.log(data);
                $('#divPanel').load(location.href + ' #divPanel');
            });
        });

        $( function() {
            $( "#searchItem" ).autocomplete({
                source: '{{ route('authors.search') }}'
            });
        } );

        $(document).on('click', '#theadLast', function orderName()
        {
            var table=$('#mytable');
            var tbody =$('#table1');

            tbody.find('tr').sort(function(a, b)
            {
                console.log('Giorgi');
                if($('#inputOrderLastName').val()=='asc')
                {
                    return $('td:first', a).text().localeCompare($('td:first', b).text(),'en-US');
                }
                else
                {
                    return $('td:first', b).text().localeCompare($('td:first', a).text(),'en-US');
                }
            }).appendTo(tbody);

            var sort_order=$('#inputOrderLastName').val();
            if(sort_order=="asc")
            {
                document.getElementById("inputOrderLastName").value="desc";
            }
            if(sort_order=="desc")
            {
                document.getElementById("inputOrderLastName").value="asc";
            }
        });
    });
</script>

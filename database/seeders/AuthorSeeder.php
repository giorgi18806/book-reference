<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AuthorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $authorsArray = [
            ['Федор', 'Михайлович', 'Достоевский'],
            ['Антон', 'Павлович', 'Чехов'],
            ['Лев', 'Николоаевич', 'Толстой'],
            ['Edgar', 'Alan', 'Poe'],

        ];
        for ($i = 0; $i < count($authorsArray); $i++) {
            DB::table('authors')->insert([
                'first_name' => $authorsArray[$i][0],
                'second_name' => $authorsArray[$i][1],
                'last_name' => $authorsArray[$i][2],
            ]);
        }
    }
}

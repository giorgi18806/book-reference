<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthorRequest;
use App\Models\Author;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $authors = Author::all();
        return view('list', compact('authors'));
    }

    /**
     * @param Request $request
     * @return string
     */
    public function create(Request $request)
    {
        Author::create($request->input());
        return 'Successfully created';
    }

    /**
     * @param Request $request
     * @return string
     */
    public function update(Request $request)
    {
        Author::where('id', $request->id)->update($request->except('_token'));

        return "Successfully updated";
    }

    /**
     * Search using jquery ui autocomplete
     *
     * @param Request $request
     * @return mixed
     */
    public function search(Request $request)
    {
        $searchTerm = $request->term;
        $searchItems = Author::where('last_name', 'LIKE', '%' . $searchTerm . '%')
            ->orWhere('first_name', 'LIKE', '%' . $searchTerm . '%')
            ->get();
        if(count($searchItems) == 0) {
            $searchResult[] = "No such author";
        }else{
            foreach ($searchItems as $key => $value) {
                $searchResult[] = $value->first_name . " " . $value->last_name;
            }
        }

        return $searchResult;
    }

    /**
     * @param Request $request
     * @return string
     */
    public function delete(Request $request)
    {
        Author::where('id', $request->id)->delete();
        return "Successfully deleted";
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Author;
use App\Models\Book;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BookController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $books = Book::all();
//        return $books;
        $authors =Author::all();
        return view('books', compact('books', 'authors'));
    }

    /**
     * @param Request $request
     * @return string
     */
    public function create(Request $request)
    {
        $data = $request->except('author_id');
        $book = Book::create($data);
        foreach ($request->author_id as $author_id) {
            DB::table('author_book')->insert([

                'author_id' => $author_id,
                'book_id' => $book->id,

            ]);
        }
        return 'Successfully created';
    }

    /**
     * @param Request $request
     * @return string
     */
    public function update(Request $request)
    {
        Book::where('id', $request->id)->update([
            'title' => $request->title,
        ]);
        foreach($request->author_id as $author_id) {
            DB::table('author_book')->where('book_id', $request->id)->update([
                'author_id' => $author_id,
            ]);
        }
        return "Successfully updated";
    }

    /**
     * Search using jquery ui autocomplete
     *
     * @param Request $request
     * @return mixed
     */
    public function search(Request $request)
    {
        $searchTerm = $request->term;
        $searchItems = Book::where('title', 'LIKE', '%' . $searchTerm . '%')->get();
        if(count($searchItems) == 0) {
            $searchResult[] = "No such book";
        }else{
            foreach ($searchItems as $key => $value) {
                $searchResult[] = $value->title . " " . $value->published_at;
            }
        }

        return $searchResult;
    }

    /**
     * @param Request $request
     * @return string
     */
    public function delete(Request $request)
    {
        Book::where('id', $request->id)->delete();
        return "Successfully deleted";
    }
}
